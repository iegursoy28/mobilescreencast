package com.sparsetechnology.screencastandroidapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.sparsetechnology.screencastandroidapp.ScreenshotService.ACTION_RECORD;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_SCREENSHOT = 59706;
    private MediaProjectionManager mgr;

    @Override
    protected void onResume() {
        super.onResume();
        String id = Constants.getCastID(this);
        if (id.isEmpty()) {
            SetCastID();
        }
    }

    private void SetCastID() {
        final EditText editText = new EditText(this);
        editText.setHint("Cast Name");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(editText);
        builder.setTitle("Set Cast ID");
        builder.setCancelable(false);
        builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String id = editText.getText().toString().trim();
                if (id.isEmpty())
                    finish();

                Constants.setCastID(MainActivity.this, id);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.create().show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mgr = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                startActivityForResult(mgr.createScreenCaptureIntent(), REQUEST_SCREENSHOT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_SCREENSHOT) {
            if (resultCode == RESULT_OK) {
                Intent i = new Intent(this, ScreenshotService.class)
                        .putExtra(ScreenshotService.EXTRA_RESULT_CODE, resultCode)
                        .putExtra(ScreenshotService.EXTRA_RESULT_INTENT, data)
                        .setAction(ACTION_RECORD);
                startService(i);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
