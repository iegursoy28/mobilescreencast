package com.sparsetechnology.screencastandroidapp;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.Date;

import ScreenCast.Sc;
import ScreenCast.ScreenCastServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class SecondFragment extends Fragment {
    private String id = "";

    private ImageView imageView;
    private TextView textView;

    private ImageViewModel imageViewModel;
    private Observer<Bitmap> bitmapObserver = new Observer<Bitmap>() {
        @Override
        public void onChanged(Bitmap bitmap) {
            if (imageView != null)
                imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, imageView.getWidth(), imageView.getHeight(), false));
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null)
            id = bundle.getString("CastID", "");

        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id.textview_castID);
        textView.setText(id);
        imageView = view.findViewById(R.id.img_cast);
        imageViewModel = new ViewModelProvider(this).get(ImageViewModel.class);
        imageViewModel.getCurrentBmp().observe(getViewLifecycleOwner(), bitmapObserver);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onStart() {
        super.onStart();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ManagedChannel channel = null;
                try {
                    channel = ManagedChannelBuilder.forTarget(Constants.SERVER_TARGET).usePlaintext().build();
                    ScreenCastServiceGrpc.ScreenCastServiceBlockingStub stub = ScreenCastServiceGrpc.newBlockingStub(channel);

                    while (true) {
                        Sc.ResponseGetScreenCast cast = stub.getScreenCast(Sc.RequestGetScreenCast.newBuilder().setId(Sc.CastID.newBuilder().setId(id).build()).build());
                        final long diff = System.currentTimeMillis() - cast.getData().getTs();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                textView.setText(id + " ( " + diff + " )");
                            }
                        });
                        Log.v("debug", "frame ts: " + cast.getData().getTs() + " diff ts: " + diff);
                        switch (cast.getData().getDataOptionCase()) {
                            case PNG_DATA:
                                SetPNGImage(imageView, cast.getData().getPngData().toByteArray());
                                break;
                            default:
                                Log.e("debug", "Not support image type");
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (channel != null)
                        channel.shutdownNow();
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void SetPNGImage(final ImageView view, final byte[] data) {
        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
        imageViewModel.getCurrentBmp().postValue(bmp);
    }

    public static class ImageViewModel extends ViewModel {
        public ImageViewModel() {
        }

        private MutableLiveData<Bitmap> currentBmp;

        public MutableLiveData<Bitmap> getCurrentBmp() {
            if (currentBmp == null) {
                currentBmp = new MutableLiveData<Bitmap>();
            }
            return currentBmp;
        }
    }

    /**
     if return fragment

     NavHostFragment.findNavController(SecondFragment.this)
     .navigate(R.id.action_SecondFragment_to_FirstFragment);
     **/
}
