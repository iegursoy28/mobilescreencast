package com.sparsetechnology.screencastandroidapp;
/***
 Copyright (c) 2015 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.

 Covered in detail in the book _The Busy Coder's Guide to Android Development_
 https://commonsware.com/Android
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.app.NotificationCompat;

import com.google.protobuf.ByteString;

import ScreenCast.Sc;
import ScreenCast.ScreenCastServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ScreenshotService extends Service {
    private static final String CHANNEL_WHATEVER = "channel_whatever";
    private static final int NOTIFY_ID = 9906;
    static final String EXTRA_RESULT_CODE = "resultCode";
    static final String EXTRA_RESULT_INTENT = "resultIntent";
    static final String ACTION_RECORD =
            BuildConfig.APPLICATION_ID + ".RECORD";
    static final String ACTION_SHUTDOWN =
            BuildConfig.APPLICATION_ID + ".SHUTDOWN";
    static final int VIRT_DISPLAY_FLAGS =
            DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY |
                    DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;
    private MediaProjection projection;
    private VirtualDisplay vdisplay;
    final private HandlerThread handlerThread =
            new HandlerThread(getClass().getSimpleName(),
                    android.os.Process.THREAD_PRIORITY_BACKGROUND);
    private Handler handler;
    private MediaProjectionManager mgr;
    private WindowManager wmgr;
    private ImageTransmogrifier it;
    private int resultCode;
    private Intent resultData;
//    final private ToneGenerator beeper =
//            new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

    private ManagedChannel channel = null;
    private ScreenCastServiceGrpc.ScreenCastServiceStub stub;
    private StreamObserver<ScreenCast.Sc.RequestScreenCast> requestStreamObserver;
    private String CastID;

    @Override
    public void onCreate() {
        super.onCreate();
        CastID = Constants.getCastID(this);

        mgr = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        wmgr = (WindowManager) getSystemService(WINDOW_SERVICE);

        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        channel = ManagedChannelBuilder.forTarget(Constants.SERVER_TARGET).usePlaintext().build();
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        if (i.getAction() == null) {
            resultCode = i.getIntExtra(EXTRA_RESULT_CODE, 1337);
            resultData = i.getParcelableExtra(EXTRA_RESULT_INTENT);
            foregroundify();
        } else if (ACTION_RECORD.equals(i.getAction())) {
            resultCode = i.getIntExtra(EXTRA_RESULT_CODE, 1337);
            resultData = i.getParcelableExtra(EXTRA_RESULT_INTENT);
            foregroundify();

            if (resultData != null) {
                startCapture();
            } else {
                Intent ui =
                        new Intent(this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(ui);
            }
        } else if (ACTION_SHUTDOWN.equals(i.getAction())) {
//            beeper.startTone(ToneGenerator.TONE_PROP_NACK);
            stopForeground(true);
            stopSelf();
        }

        return (START_NOT_STICKY);
    }

    @Override
    public void onDestroy() {
        stopCapture();

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new IllegalStateException("Binding not supported. Go away.");
    }

    WindowManager getWindowManager() {
        return (wmgr);
    }

    Handler getHandler() {
        return (handler);
    }

    StreamObserver<Sc.ResponseStartScreenCast> responseObserver = new StreamObserver<Sc.ResponseStartScreenCast>() {
        @Override
        public void onNext(Sc.ResponseStartScreenCast value) {

        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onCompleted() {

        }
    };

    long lastSendTs = 0;

    void processImage(final byte[] png) {
        if ((System.currentTimeMillis() - lastSendTs) < 1000)
            return;

        try {
            lastSendTs = System.currentTimeMillis();
            Log.v("debug", "image size: " + png.length);
            if (requestStreamObserver != null)
                requestStreamObserver.onNext(Sc.RequestScreenCast.newBuilder()
                        .setId(Sc.CastID.newBuilder().setId(CastID).build())
                        .setData(Sc.ScreenData.newBuilder()
                                .setTs(lastSendTs)
                                .setPngData(ByteString.copyFrom(png)).build())
                        .build());
        } catch (Exception ignored) {
        }

//        beeper.startTone(ToneGenerator.TONE_PROP_ACK);
//        stopCapture();
    }

    private void stopCapture() {
        if (projection != null) {
            projection.stop();
            vdisplay.release();
            projection = null;
        }

        if (requestStreamObserver != null)
            requestStreamObserver.onCompleted();
    }

    private void startCapture() {
        stub = ScreenCastServiceGrpc.newStub(channel);
        requestStreamObserver = stub.startScreenCast(responseObserver);

        projection = mgr.getMediaProjection(resultCode, resultData);
        it = new ImageTransmogrifier(this);

        MediaProjection.Callback cb = new MediaProjection.Callback() {
            @Override
            public void onStop() {
                vdisplay.release();
            }
        };

        vdisplay = projection.createVirtualDisplay("andshooter",
                it.getWidth(), it.getHeight(),
                getResources().getDisplayMetrics().densityDpi,
                VIRT_DISPLAY_FLAGS, it.getSurface(), null, handler);

        projection.registerCallback(cb, handler);
    }

    private void foregroundify() {
        NotificationManager mgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                mgr.getNotificationChannel(CHANNEL_WHATEVER) == null) {
            mgr.createNotificationChannel(new NotificationChannel(CHANNEL_WHATEVER,
                    "Whatever", NotificationManager.IMPORTANCE_DEFAULT));
        }

        NotificationCompat.Builder b =
                new NotificationCompat.Builder(this, CHANNEL_WHATEVER);

        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);

        b.setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(getString(R.string.app_name));

//        b.addAction(android.R.drawable.ic_media_play,
//                /*getString(R.string.notify_record)*/ "notify_record",
//                buildPendingIntent(ACTION_RECORD));

        b.addAction(android.R.drawable.ic_media_pause,
                /*getString(R.string.notify_shutdown)*/ "shutdown",
                buildPendingIntent(this, ACTION_SHUTDOWN));

        startForeground(NOTIFY_ID, b.build());
    }

    public static PendingIntent buildPendingIntent(Context context, String action) {
        Intent i = new Intent(context, ScreenshotService.class);
        i.setAction(action);

        return (PendingIntent.getService(context, 0, i, 0));
    }
}