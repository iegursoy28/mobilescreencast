package com.sparsetechnology.screencastandroidapp;

import android.content.Context;
import android.content.SharedPreferences;

public class Constants {
    public static final String SERVER_TARGET = /*"10.5.178.141:15123"*/ "40.127.152.22:5023";

    public static String getCastID(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("Const", Context.MODE_PRIVATE);
        return preferences.getString("CastID", "");
    }

    public static void setCastID(Context context, String castID) {
        SharedPreferences preferences = context.getSharedPreferences("Const", Context.MODE_PRIVATE);
        preferences.edit().putString("CastID", castID).apply();
    }
}
