package com.sparsetechnology.screencastandroidapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import ScreenCast.Sc;
import ScreenCast.ScreenCastServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

@SuppressLint("StaticFieldLeak")
public class FirstFragment extends Fragment {
    private Context mContext;
    private ListView listView;
    private SwipeRefreshLayout refreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = inflater.getContext();
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @SuppressLint("StaticFieldLeak")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = view.findViewById(R.id.lstCast);
        refreshLayout = view.findViewById(R.id.refresh_layout);
        refreshLayout.setRefreshing(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new UpdateTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                if (view instanceof TextView)
                    bundle.putString("CastID", ((TextView) view).getText().toString());

                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
            }
        });

        new UpdateTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class UpdateTask extends AsyncTask<Void, Void, Void> {
        Sc.ResponseGetCastList list;

        @Override
        protected Void doInBackground(Void... voids) {
            ManagedChannel channel = null;
            try {
                channel = ManagedChannelBuilder.forTarget(Constants.SERVER_TARGET).usePlaintext().build();
                ScreenCastServiceGrpc.ScreenCastServiceBlockingStub stub = ScreenCastServiceGrpc.newBlockingStub(channel);
                list = stub.getCastList(Sc.RequestGetCastList.newBuilder().build());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (channel != null)
                    channel.shutdownNow();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            List<String> l = new ArrayList<>();
            for (int i = 0; i < list.getListCount(); i++)
                l.add(list.getList(i).getId());

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                    (mContext, android.R.layout.simple_list_item_1, android.R.id.text1, l);
            listView.setAdapter(arrayAdapter);
            refreshLayout.setRefreshing(false);
        }
    }

    ;
}
