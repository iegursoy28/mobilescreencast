using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using ScreenCast;

namespace ScreenCastServer
{
    public class ScreenCastServiceImpl : ScreenCast.ScreenCastService.ScreenCastServiceBase
    {
        private readonly object myLock = new object();

        private Dictionary<string, ScreenCast.ScreenData> castMap = new Dictionary<string, ScreenData>();

        private readonly ILogger<ScreenCastServiceImpl> _logger;
        public ScreenCastServiceImpl(ILogger<ScreenCastServiceImpl> logger)
        {
            _logger = logger;
        }

        public override Task<ResponseGetCastList> GetCastList(RequestGetCastList request, ServerCallContext context)
        {
            var castList = new ScreenCast.ResponseGetCastList();
            foreach (var item in castMap)
                castList.List.Add(new CastID() { Id = item.Key });

            return Task.FromResult(castList);
        }

        public override Task<ResponseGetScreenCast> GetScreenCast(RequestGetScreenCast request, ServerCallContext context)
        {
            lock (myLock)
            {
                if (castMap.ContainsKey(request.Id.Id))
                    return Task.FromResult(new ResponseGetScreenCast() { Data = castMap[request.Id.Id] });
            }

            return Task.FromException<ResponseGetScreenCast>(new RpcException(new Status(StatusCode.NotFound, "")));
        }

        public override async Task<ResponseStartScreenCast> StartScreenCast(IAsyncStreamReader<RequestScreenCast> requestStream, ServerCallContext context)
        {
            string id = "";
            try
            {
                while (await requestStream.MoveNext())
                {
                    var c = requestStream.Current;

                    lock (myLock)
                    {
                        if (!castMap.ContainsKey(c.Id.Id))
                            castMap.Add(c.Id.Id, c.Data);
                        else
                            castMap[c.Id.Id] = c.Data;
                    }
                    id = c.Id.Id;
                    System.Console.WriteLine(c.Data.Ts);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
            finally
            {
                if (id != null && !string.IsNullOrEmpty(id))
                {
                    lock (myLock)
                    {
                        castMap.Remove(id);
                    }
                    System.Console.WriteLine("Removed id: " + id);
                }
            }

            return new ResponseStartScreenCast();
        }
    }
}
